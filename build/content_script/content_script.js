(function () {
'use strict';

const Helpers = {};

Helpers.debounce = (func, wait = 100) => {
  let timeout;
  return function(...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      func.apply(this, args);
    }, wait);
  };
};

//Returns desinformation type as string.
Helpers.desinformationType = arr => {
  let finalString = "";
  arr.forEach((el, index, array) => {
    let translation = chrome.i18n.getMessage(el);
    if (translation) {
      finalString += translation;

      if (array.length !== index + 1) {
        finalString += ", ";
      }
    }
  });
  return finalString;
};

function banner() {
  let obj = blockedUrls[document.domain.replace("www.", "")];
  let autor = obj["markedBy"];
  let el = document.createElement("DIV");
  let type = Helpers.desinformationType(obj["type"]);

  el.classList.add("faker_banner");
  el.innerHTML =
    '<div class="faker_banner-container"><span class="faker_banner-ic"></span><span class="faker_banner-content">' +
    chrome.i18n.getMessage("BannerMainMessage", type) +
    " " +
    autor +
    '<a href="http://' +
    autor +
    '"> ' +
    chrome.i18n.getMessage("BannerMainMessageLink") +
    "</a>" +
    "</span> <div class='faker_banner-close'>" +
    chrome.i18n.getMessage("BannerClose") +
    "</div></div>";

  document.body.appendChild(el);
  document
    .querySelector(".faker_banner-close")
    .addEventListener("click", () => {
      el.classList.add("closed");
    });
}

function verificationfb(event) {
  document
    .querySelectorAll(".userContentWrapper .ellipsis:not(.faker_flagged)")
    .forEach(element => {
      var text = element.innerText.trim().toLowerCase();
      let item = blockedUrls[text];

      if (item) {
        let autor = item["markedBy"];
        let type = Helpers.desinformationType(item["type"]);
        element.innerHTML =
          text +
          " " +
          '<span class="faker_fb-flag"><span class="faker_fb-flag-ic"></span> ' +
          chrome.i18n.getMessage("FacebookMainMessage", type) +
          " " +
          autor +
          "</span>";
      }
      element.classList.add("faker_flagged");
    });
}

function verificationtw(event) {
  document
    .querySelectorAll(
      ".js-macaw-cards-iframe-container iframe:not(.faker_flagged)"
    )
    .forEach(MainElement => {
      MainElement.contentWindow.document
        .querySelectorAll(".SummaryCard-destination")
        .forEach(element => {
          var text = element.innerText.trim().toLowerCase();
          let item = blockedUrls[text];
          let div = document.createElement("DIV");
          if (item) {
            let autor = item["markedBy"];
            let type = Helpers.desinformationType(item["type"]);
            div.innerHTML =
              '<span style="display: block; display: -ms-flexbox; display: flex; -ms-flex-align: center; align-items: center; font-size: 12px; padding: 0px 6px; background-color: #fbeaca; border-radius: 4px; margin: -7px 0px 6px 0; justify-content: center;">' +
              chrome.i18n.getMessage("FacebookMainMessage", type) +
              " " +
              autor +
              "</span>";

            element.parentNode.insertBefore(div, element.parentNode.firstChild);
          }
          MainElement.classList.add("faker_flagged");
        });
    });
}

let domain = document.domain.replace("www.", "");

if (domain === "facebook.com") {
  verificationfb(); //Run on first frame

  //Add scroll listener
  var debouncedfb = Helpers.debounce(verificationfb, 400);
  window.addEventListener("scroll", debouncedfb);
}

if (domain === "twitter.com") {
  verificationtw(); //Run on first frame

  //Add scroll listener
  var debouncedtw = Helpers.debounce(verificationtw, 400);
  window.addEventListener("scroll", debouncedtw);
}

if (blockedUrls[domain] != undefined) {
  banner();
}

}());
