## Jak nainstalovat vývojařskou verzi

1.  Stáhnout tento repozitář.
2.  Otevřít chrome a přejít na `chrome://extensions/`.
3.  Zapnout `developer mode`.
4.  Nahrát rozbalené rozšíření ze složky `/build/`.

![Cesta pro instalaci vyvojařské verze chrome rozšíření](docs/tutorial1.png)
