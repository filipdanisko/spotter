import Helpers from "./helpers";

function verificationfb(event) {
  document
    .querySelectorAll(".userContentWrapper .ellipsis:not(.faker_flagged)")
    .forEach(element => {
      var text = element.innerText.trim().toLowerCase();
      let item = blockedUrls[text];

      if (item) {
        let autor = item["markedBy"];
        let type = Helpers.desinformationType(item["type"]);
        element.innerHTML =
          text +
          " " +
          '<span class="faker_fb-flag"><span class="faker_fb-flag-ic"></span> ' +
          chrome.i18n.getMessage("FacebookMainMessage", type) +
          " " +
          autor +
          "</span>";
      }
      element.classList.add("faker_flagged");
    });
}

export default verificationfb;
