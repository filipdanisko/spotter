import Helpers from "./helpers";
function verificationtw(event) {
  document
    .querySelectorAll(
      ".js-macaw-cards-iframe-container iframe:not(.faker_flagged)"
    )
    .forEach(MainElement => {
      MainElement.contentWindow.document
        .querySelectorAll(".SummaryCard-destination")
        .forEach(element => {
          var text = element.innerText.trim().toLowerCase();
          let item = blockedUrls[text];
          let div = document.createElement("DIV");
          if (item) {
            let autor = item["markedBy"];
            let type = Helpers.desinformationType(item["type"]);
            div.innerHTML =
              '<span style="display: block; display: -ms-flexbox; display: flex; -ms-flex-align: center; align-items: center; font-size: 12px; padding: 0px 6px; background-color: #fbeaca; border-radius: 4px; margin: -7px 0px 6px 0; justify-content: center;">' +
              chrome.i18n.getMessage("FacebookMainMessage", type) +
              " " +
              autor +
              "</span>";

            element.parentNode.insertBefore(div, element.parentNode.firstChild);
          }
          MainElement.classList.add("faker_flagged");
        });
    });
}

export default verificationtw;
