import Helpers from "./helpers";

function banner() {
  let obj = blockedUrls[document.domain.replace("www.", "")];
  let autor = obj["markedBy"];
  let el = document.createElement("DIV");
  let type = Helpers.desinformationType(obj["type"]);

  el.classList.add("faker_banner");
  el.innerHTML =
    '<div class="faker_banner-container"><span class="faker_banner-ic"></span><span class="faker_banner-content">' +
    chrome.i18n.getMessage("BannerMainMessage", type) +
    " " +
    autor +
    '<a href="http://' +
    autor +
    '"> ' +
    chrome.i18n.getMessage("BannerMainMessageLink") +
    "</a>" +
    "</span> <div class='faker_banner-close'>" +
    chrome.i18n.getMessage("BannerClose") +
    "</div></div>";

  document.body.appendChild(el);
  document
    .querySelector(".faker_banner-close")
    .addEventListener("click", () => {
      el.classList.add("closed");
    });
}

export default banner;
