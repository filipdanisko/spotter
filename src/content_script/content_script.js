import Banner from "./banner";
import Helpers from "./helpers";
import Facebook from "./facebook";
import Twitter from "./twitter";

let domain = document.domain.replace("www.", "");

if (domain === "facebook.com") {
  Facebook(); //Run on first frame

  //Add scroll listener
  var debouncedfb = Helpers.debounce(Facebook, 400);
  window.addEventListener("scroll", debouncedfb);
}

if (domain === "twitter.com") {
  Twitter(); //Run on first frame

  //Add scroll listener
  var debouncedtw = Helpers.debounce(Twitter, 400);
  window.addEventListener("scroll", debouncedtw);
}

if (blockedUrls[domain] != undefined) {
  Banner();
}
