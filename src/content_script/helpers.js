const Helpers = {};

Helpers.debounce = (func, wait = 100) => {
  let timeout;
  return function(...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      func.apply(this, args);
    }, wait);
  };
};

//Returns desinformation type as string.
Helpers.desinformationType = arr => {
  let finalString = "";
  arr.forEach((el, index, array) => {
    let translation = chrome.i18n.getMessage(el);
    if (translation) {
      finalString += translation;

      if (array.length !== index + 1) {
        finalString += ", ";
      }
    }
  });
  return finalString;
};

export default Helpers;
