const https = require("https");
const parser = require("xml2json");
const fs = require("fs");

const konspiratoriURL =
  "https://www.konspiratori.sk/assets/downloads/zoznam.xml";
const opensourcesURL =
  "https://raw.githubusercontent.com/BigMcLargeHuge/opensources/master/sources/sources.json";

function normFeedKonsipratori() {
  return new Promise(resolve => {
    let normalizedObj = {};

    https.get(konspiratoriURL, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        let json = parser.toJson(body);
        let data = JSON.parse(json).root.row;
        data.forEach(el => {
          normalizedObj[el.nazov.trim()] = {
            type: ["misleading"],
            markedBy: "konspiratori.sk"
          };
        });
        resolve(normalizedObj);
      });
    });
  });
}

function normFeedOpensources() {
  return new Promise(resolve => {
    let normalizedObj = {};

    https.get(opensourcesURL, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        let data = JSON.parse(body);
        for (let key in data) {
          let types = [];
          if (data[key]["type"] != "") {
            types.push(data[key]["type"]);
          }
          if (data[key]["2nd type"] != "") {
            types.push(data[key]["2nd type"]);
          }
          if (data[key]["3rd type"] != "") {
            types.push(data[key]["3rd type"]);
          }

          //drop reliable and only political sources
          if (
            types.includes("reliable") ||
            (types.includes("political") && types.length === 1)
          ) {
            console.log("Dropping source, reliable or only political");
          } else {
            normalizedObj[key.trim()] = {
              type: types,
              markedBy: "opensources.co"
            };
          }
        }
        resolve(normalizedObj);
      });
    });
  });
}

async function getData() {
  let a = await normFeedKonsipratori();
  let b = await normFeedOpensources();
  let obj = { ...a, ...b };
  return obj;
}

getData().then(data => {
  fs.writeFile(
    "build/data.js",
    "var blockedUrls = " + JSON.stringify(data, null),
    () => {
      console.log("Written to /build/");
      return;
    }
  );
});
