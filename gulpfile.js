let gulp = require("gulp");
let path = require("path");

//Scripts
let uglify = require("gulp-uglify");
let pump = require("pump");
let rollup = require("gulp-rollup");

//Less
let less = require("gulp-less");
let cssnano = require("gulp-cssnano");
let LessAutoprefix = require("less-plugin-autoprefix");
let autoprefix = new LessAutoprefix({
  browsers: ["last 2 versions"]
});

//Images
let imagemin = require("gulp-imagemin");
let imageminJpegtran = require("imagemin-jpegtran");

//scripts
gulp.task("scripts", () => {
  gulp
    .src("./src/**/*.js")
    .pipe(
      rollup({
        format: "iife",
        plugins: [],
        entry: ["./src/content_script/content_script.js"]
      })
    )
    .pipe(gulp.dest("./build/"));
});

gulp.task("compress", ["scripts"], cb => {
  // pump([gulp.src("./build/**/*.js"), uglify(), gulp.dest("./build/")], cb);
});

//styles
gulp.task("less", () => {
  return gulp
    .src("./src/**/*.less")
    .pipe(less({ plugins: [autoprefix] }))
    .pipe(gulp.dest("./build/"));
});

//Image optim
gulp.task("optimImages", () =>
  gulp
    .src("src/images/*")
    .pipe(
      imagemin([
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 })
      ])
    )
    .pipe(gulp.dest("src/images/"))
);

//Move items
gulp.task("html", () => {
  return gulp.src("./src/**/*.html").pipe(gulp.dest("./build/"));
});
gulp.task("manifest", () => {
  return gulp.src("./src/manifest/*").pipe(gulp.dest("./build/"));
});
gulp.task("images", () =>
  gulp.src("src/images/*").pipe(gulp.dest("./build/images/"))
);
gulp.task("locales", () =>
  gulp.src("src/_locales/**/*").pipe(gulp.dest("./build/_locales"))
);
//Watch
gulp.task("watch", function() {
  gulp.watch("./src/**/*.js", ["compress"]);
  gulp.watch("./src/**/*.less", ["less"]);
  gulp.watch("./src/manifest/*.json", ["manifest"]);
  gulp.watch("./src/images/*", ["images"]);
  gulp.watch("./src/**/*.html", ["html"]);
  gulp.watch("./src/_locales/**/*.json", ["locales"]);
});

gulp.task("default", [
  "watch",
  "compress",
  "less",
  "images",
  "html",
  "manifest",
  "optimImages",
  "locales"
]);
